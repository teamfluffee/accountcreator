package scripts.modules.accountcreator.nodes.decisionnodes;

import scripts.modules.accountcreator.Variables;
import scripts.modules.accountcreator.nodes.processnodes.CreateAccount;
import scripts.modules.accountcreator.nodes.processnodes.SolveCaptcha;
import scripts.modules.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.FactoryDecisionNode;

public class ShouldSolveCaptcha extends FactoryDecisionNode {

    private Variables variables;

    private ShouldSolveCaptcha() {}

    @Override
    public boolean isValid() {
        return !variables.getCaptchaSolver().isCaptchaSolved();
    }

    @Override
    public void initializeNode() {
        setTrueNode(new SolveCaptcha(variables.getCaptchaSolver()));
        setFalseNode(new CreateAccount(variables));
    }

    public static ShouldSolveCaptcha create(Variables variables) {
        ShouldSolveCaptcha shouldSolveCaptcha = new ShouldSolveCaptcha();
        shouldSolveCaptcha.variables = variables;
        shouldSolveCaptcha.initializeNode();
        return shouldSolveCaptcha;
    }
}
