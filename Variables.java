package scripts.modules.accountcreator;

import okhttp3.OkHttpClient;
import scripts.modules.fluffeesapi.data.accounts.AccountDetails;
import scripts.modules.fluffeesapi.data.network.ConnectionSettings;
import scripts.modules.fluffeesapi.data.structures.PrivateSingleton;
import scripts.modules.fluffeesapi.web.RSCookieJar;
import scripts.modules.fluffeesapi.web.accountCreation.captcha.CaptchaSolver;

public class Variables extends PrivateSingleton {

    private AccountDetails accountDetails;
    private ConnectionSettings connectionSettings;
    private CaptchaSolver captchaSolver;
    private boolean creationCompleted;
    private OkHttpClient httpClient;

    public Variables(Class callingClass,
                     AccountDetails accountDetails,
                     ConnectionSettings connectionSettings,
                     CaptchaSolver captchaSolver) throws IllegalAccessException {
        super(callingClass);
        this.accountDetails = accountDetails;
        this.connectionSettings = connectionSettings;
        this.captchaSolver = captchaSolver;

        OkHttpClient.Builder builder = new OkHttpClient.Builder().cookieJar(new RSCookieJar());
        if (connectionSettings.getProxy() != null) {
            builder.proxy(connectionSettings.getProxy())
                    .addInterceptor(connectionSettings.getInterceptor());
        }

        this.httpClient = builder.build();
        this.creationCompleted = false;
    }

    public AccountDetails getAccountDetails() {
        return accountDetails;
    }

    public ConnectionSettings getConnectionSettings() {
        return connectionSettings;
    }

    public CaptchaSolver getCaptchaSolver() {
        return captchaSolver;
    }

    public OkHttpClient getHttpClient() {
        return httpClient;
    }

    public boolean isCreationCompleted() {
        return creationCompleted;
    }

    public void setCreationCompleted(boolean creationCompleted) {
        this.creationCompleted = creationCompleted;
    }

    @Override
    public Class getAllowedClass() {
        return AccountCreator.class;
    }
}
