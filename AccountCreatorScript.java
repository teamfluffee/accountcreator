package scripts.modules.accountcreator;

import org.tribot.script.ScriptManifest;
import org.tribot.script.interfaces.Arguments;
import org.tribot.script.interfaces.Painting;
import org.tribot.script.interfaces.Starting;
import scripts.modules.fluffeesapi.data.accounts.AccountCreationDetails;
import scripts.modules.fluffeesapi.data.accounts.AccountDetails;
import scripts.modules.fluffeesapi.data.network.ConnectionSettings;
import scripts.modules.fluffeesapi.scripting.frameworks.mission.missiontypes.Mission;
import scripts.modules.fluffeesapi.scripting.frameworks.mission.scriptTypes.MissionScript;
import scripts.modules.fluffeesapi.scripting.painting.scriptpaint.ScriptPaint;
import scripts.modules.fluffeesapi.utilities.ArgumentUtilities;
import scripts.modules.fluffeesapi.web.accountCreation.captcha.CaptchaSolver;

import java.util.HashMap;

@ScriptManifest(
        authors     = "Fluffee",
        category    = "Tools",
        name        = "Fluffees Account Creator",
        version     = 1.0,
        description = "Creates accounts using the following arguments. <br>" +
                "accountEmail:email_address;accountPassword:password;;proxyIP:192.168.0.1;proxyPort:1080;" +
                "proxyUsername:username;proxyPassword:password;useProxyCaptcha:true;captchaSolver:ANTI_CAPTCHA;" +
                "captchaKey:captchaKey;",
        gameMode = 1)

public class AccountCreatorScript extends MissionScript implements Starting, Arguments, Painting {

    private AccountDetails accountDetails = null;
    private ConnectionSettings connectionSettings = null;
    private CaptchaSolver captchaSolver = null;

    @Override
    public void passArguments(HashMap<String, String> hashMap) {
        if (hashMap.isEmpty()) {
            return;
        }
        HashMap<String, String> arguments = ArgumentUtilities.get(hashMap);

        if (arguments.size() < 3) {
            return;
        }

        captchaSolver = CaptchaSolver.getCaptchaSolver(
                arguments.get("captchaSolver").toUpperCase().replace("_", ""),
                arguments.get("captchaKey"));
        connectionSettings = new ConnectionSettings(
                arguments.getOrDefault("proxyIP", ""),
                Integer.parseInt(arguments.getOrDefault("proxyPort", "")),
                arguments.getOrDefault("proxyUsername", ""),
                arguments.getOrDefault("proxyPassword", "")
        );
        accountDetails = new AccountDetails(
                arguments.get("accountEmail"),
                arguments.get("accountPassword"),
                AccountCreationDetails.randomFactory(1)
        );
    }

    @Override
    public void onStart() {
        this.setLoginBotState(false);
    }

    @Override
    public void preScriptTasks() {
        this.setScriptPaint(
            new ScriptPaint.Builder(
                    ScriptPaint.hex2Rgb("#ffb140"), "Account Creator"
                ).addField("Version", Double.toString(1.00))
            .build()
        );
    }

    @Override
    public Mission getMission() {
        if (accountDetails == null || connectionSettings == null || captchaSolver == null) {
            return new Mission.EarlyEndMission("An invalid set of arguments were passed, please consult the script description to find valid arguments");
        }
        return new AccountCreator(accountDetails, connectionSettings, captchaSolver);
    }
}
